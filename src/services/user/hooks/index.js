'use strict';

const globalHooks = require('../../../hooks');
const hooks = require('feathers-hooks');
const auth = require('feathers-authentication');
const legacyauth = require('feathers-legacy-authentication-hooks');
const permissions = require('feathers-permissions');

exports.before = {
  all: [],
  find: [
    legacyauth.verifyToken(),
    legacyauth.populateUser(),
    legacyauth.restrictToAuthenticated()
  ],
  get: [
    legacyauth.verifyToken(),
    legacyauth.populateUser(),
    legacyauth.restrictToAuthenticated(),
    legacyauth.restrictToOwner({ ownerField: '_id' })
  ],
  create: [
    legacyauth.hashPassword()
  ],
  update: [
    legacyauth.verifyToken(),
    legacyauth.populateUser(),
    legacyauth.restrictToAuthenticated(),
    legacyauth.restrictToOwner({ ownerField: '_id' })
  ],
  patch: [
    legacyauth.verifyToken(),
    legacyauth.populateUser(),
    legacyauth.restrictToAuthenticated(),
    legacyauth.restrictToOwner({ ownerField: '_id' })
  ],
  remove: [
    legacyauth.verifyToken(),
    legacyauth.populateUser(),
    legacyauth.restrictToAuthenticated(),
    legacyauth.restrictToOwner({ ownerField: '_id' })
  ]
};

exports.after = {
  all: [hooks.remove('password')],
  find: [],
  get: [],
  create: [],
  update: [],
  patch: [],
  remove: []
};
