'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('food service', function() {
  it('registered the foods service', () => {
    assert.ok(app.service('foods'));
  });
});
