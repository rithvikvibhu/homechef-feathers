'use strict';

const globalHooks = require('../../../hooks');
// const hooks = require('feathers-hooks');
const hooks = require('feathers-hooks-common');
const auth = require('feathers-authentication');
const legacyauth = require('feathers-legacy-authentication-hooks');
const permissions = require('feathers-permissions');

function appendSellerId(hook) {
  // console.log('Hook: ', hook);
  hook.data.sellerId = hook.params.user._id;
  hook.data.createdDate = new Date();
}
function populateSeller(hook) {
  const promises = hook.result.data.map( (element, index, array) => {
    return hook.app.services.users.get(element.sellerId).then((user)=> {
      console.log(hook.result.data[index].name, user);
      hook.result.data[index].sellerName = user.name;
    });
  });
  return Promise.all(promises).then(() =>  hook);
}
exports.before = {
  all: [
    // legacyauth.verifyToken(),
    // legacyauth.populateUser(),
    // legacyauth.restrictToAuthenticated()
    auth.hooks.authenticate('jwt')
    // permissions.hooks.checkPermissions({ service: 'users' }),
    // permissions.hooks.isPermitted()
  ],
  find: [],
  get: [],
  create: [appendSellerId],
  update: [],
  patch: [],
  remove: []
};

exports.after = {
  all: [],
  find: [populateSeller],
  get: [],
  create: [],
  update: [],
  patch: [],
  remove: []
};
