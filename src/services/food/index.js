'use strict';

const path = require('path');
const NeDB = require('nedb');
const service = require('feathers-nedb');
const hooks = require('./hooks');

module.exports = function(){
  const app = this;

  const db = new NeDB({
    filename: path.join(app.get('nedb'), 'foods.db'),
    autoload: true
  });

  let options = {
    Model: db,
    paginate: {
      default: 50,
      max: 50
    }
  };

  // Initialize our service with any options it requires
  app.use('/foods', service(options));

  // Get our initialize service to that we can bind hooks
  const foodService = app.service('/foods');

  // Set up our before hooks
  foodService.before(hooks.before);

  // Set up our after hooks
  foodService.after(hooks.after);
};
