FROM node:alpine
WORKDIR /home/feathers

#ENV DEBUG '*'

COPY package.json package.json
RUN npm install --production

COPY config/ config/
COPY public/ public/
COPY src/ src/
COPY data/ data/

EXPOSE 8080

ENV NODE_ENV 'production'
ENV PORT '8080'
CMD ["node", "src/index.js"]
